/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author LAB_CCBB FD#8
 */
public class Empleado {
    String PrimNombre;
    String SegNombre;
    String PrimApellido;
    String SegApellido;
    String DiaNac;
    String Oficio;
    String DiaContra;
    int AñosExp;

    public Empleado() {
    }

    public Empleado(String PrimNombre, String SegNombre, String PrimApellido, String SegApellido, String DiaNac, String MesNac, String AñoNac, String Oficio, String DiaContra, String MesContra, String AñoContra, int AñosExp) {
        this.PrimNombre = PrimNombre;
        this.SegNombre = SegNombre;
        this.PrimApellido = PrimApellido;
        this.SegApellido = SegApellido;
        this.DiaNac = DiaNac;
        this.Oficio = Oficio;
        this.DiaContra = DiaContra;
        this.AñosExp = AñosExp;
    }

    public String getPrimNombre() {
        return PrimNombre;
    }

    public void setPrimNombre(String PrimNombre) {
        this.PrimNombre = PrimNombre;
    }

    public String getSegNombre() {
        return SegNombre;
    }

    public void setSegNombre(String SegNombre) {
        this.SegNombre = SegNombre;
    }

    public String getPrimApellido() {
        return PrimApellido;
    }

    public void setPrimApellido(String PrimApellido) {
        this.PrimApellido = PrimApellido;
    }

    public String getSegApellido() {
        return SegApellido;
    }

    public void setSegApellido(String SegApellido) {
        this.SegApellido = SegApellido;
    }

    public String getDiaNac() {
        return DiaNac;
    }

    public void setDiaNac(String DiaNac) {
        this.DiaNac = DiaNac;
    }

    public String getOficio() {
        return Oficio;
    }

    public void setOficio(String Oficio) {
        this.Oficio = Oficio;
    }

    public String getDiaContra() {
        return DiaContra;
    }

    public void setDiaContra(String DiaContra) {
        this.DiaContra = DiaContra;
    }

    public int getAñosExp() {
        return AñosExp;
    }

    public void setAñosExp(int AñosExp) {
        this.AñosExp = AñosExp;
    }
    
    
}
