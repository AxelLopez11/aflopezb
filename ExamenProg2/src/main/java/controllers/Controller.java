/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.swing.JFileChooser;
import javax.swing.Timer;
import models.Empleado;
import views.MainFrame;

/**
 *
 * @author LAB_CCBB FD#8
 */
public class Controller implements ActionListener{
    private MainFrame datosf;
    private Empleado da;
    private Timer t;
    private ActionListener a;
    JFileChooser d;
    int i=0;
    public Controller(MainFrame df){
        datosf = df;
        d = new JFileChooser();
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "Save":
                d.showSaveDialog(datosf);
                File file = d.getSelectedFile();
                da = datosf.getData();
                save(file);
                break;
            case "Clean":
                datosf.Limpiar();
                break;
            case "Load":
                d.showOpenDialog(datosf);
                da = open(d.getSelectedFile());
                datosf.setData(da);
                break;
        }
    }
    
    public void save(File file){
        try{
           ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
           w.writeObject(da);
           w.flush();
        }catch(IOException ex){
            
        }
    }
    
    public Empleado open(File file){
        try{
           ObjectInputStream oi = new ObjectInputStream(new FileInputStream(file));
           return (Empleado)oi.readObject();
        }catch(IOException|ClassNotFoundException ex){
            
        }
        return null;
    }
}
    

